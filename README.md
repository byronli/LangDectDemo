LangDectDemo：
    
    简单的NLP语言检测算法实现的Demo
    
    
下载配置：

    git clone 后需要重新配置param/common_param.py 中的DATAPATH. 添加新的语言数据后需要在Data目录下添加新的语言子目录，并在common_param.py 中LANGUAGES中添加新的语言类型。
    

目录：

    --data : 数据目录 
            --CHINESE: 所要识别语言的目录
            --ENGLISH：...
        
    --param: 项目的整体参数配置，包括数据路径等。
    
    --src  : 各种language dection方法的实现，如n-gram在src的ngram文件夹下实现。
            --ngram n-gram方法的实现。
         
    --test : 测试文件目录
    
    --util : 工具类目录，包括分词以及IO操作。
    
    --thirdpary ： 第三方依赖包，主要是分词工具。
        

TODO：

    （1）修改util下的wordsplit.py中的split_run来支持多种语言的分词操作。
    （2）补充数据，完善test下的approach_test.py，通过交叉验证来测试。
    （OPTIONAL）修改ngram的train方法，使之支持多线程训练。
    
MAYBE FURTHER：

    其它方法的实现，最后进行assmeble。



附录：
	分词工具 ： jieba and nltk.
	语料     ： 
		    中文 : netease 新闻 
	    	英文 : https://github.com/nlp-compromise/nlp-corpus


Experimental Result:

	ACCURACY : 91%
	ENGLISH vs CHINESE
	K, and N are tested in testfiles.
