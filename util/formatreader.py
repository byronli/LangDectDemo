import sys
import codecs

import os

class formatreader:

	def __init__(self, path):
		self.path = path;

	def readlines(self):
		lines = []
                if(os.path.isdir(self.path)):
		    for root, dirs, files in os.walk(self.path):  
		        filepath = self.path
    			for filename in files:
        			filepath = filepath+"/"+filename
				with codecs.open(filepath,'r',encoding='utf8') as f:
        				flines = f.readlines()
					f.close()
				lines = lines+flines

                elif(os.path.isfile(self.path)):
                    with codecs.open(self.path,'r',encoding='utf8') as f:
        		lines = f.readlines()
                        f.close()
                else:
                    lines = []

		return lines
