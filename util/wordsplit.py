# -*- coding: utf-8 -*-
import sys
sys.path.append("../thirdparty/jieba")
sys.path.append("../param/")

import threading
import formatreader as fr
import codecs

import jieba as jb
import nltk  as nk

def split_run(lang_type, thread_text, th_id, start, length, lines):
	words = []	
	for i in range(length):

            if(lang_type == "CHINESE"):
                seg_list = jb.cut(lines[start + i], cut_all=False)
            else:
                seg_list = nk.word_tokenize(lines[start + i])

	    for word in seg_list:
		words.append(word);

	thread_text[th_id] = words;

# path : file path.
# mode : threads number.
# Multi-threading wordsplit.
def split(path, lang_type, mode):   
	text = [];
	thread_text = {};
	if(mode <= 0):
		return text;

	freader = fr.formatreader(path);
	lines = freader.readlines();
        #with codecs.open(path,'r',encoding='utf8') as f:
        #	lines = f.readlines()

	if(len(lines) < 1):
		return [];


	sub_len = (len(lines)/mode)	
	if(sub_len < 1):
		sub_len = 1;

	start       = 0
	counter     = 0;
	thread_list = []

	while(start < len(lines)):
		t = threading.Thread(target=split_run, args=(lang_type, thread_text, counter, start, sub_len, lines))
		counter += 1;

		start = start + sub_len;
		if((len(lines) - start) < sub_len):
			sub_len = len(lines) - start 

    		thread_list.append(t)	


	for thread in thread_list:
    		thread.start()

	for thread in thread_list:
    		thread.join()

	for val in (thread_text.itervalues()):
		text = text + val;

	return text;
