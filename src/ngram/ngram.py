# -*- coding: utf-8 -*-
import sys
sys.path.append("../../util")
sys.path.append("../../param")
sys.path.append("../../thirdparty/jieba")

import pickle
import hashlib
import operator
import threading as thd
import wordsplit as sp
import common_param as param 
import ngram_param as nparam

from nltk.tokenize import word_tokenize
from collections import defaultdict
from approach import LangDectApproach

class ngram_approach(LangDectApproach):	

	#pass data is dict with key:Lang, val:Data.
	def __init__(self, data):
		self.cate_data = data
		self.profiles  = {}

	def __build_profile(self):
		profile = {}
		hash_tb = {} 
		hash_tb = defaultdict(lambda:0,hash_tb)

		data = self.cur_data
		if(len(data)<nparam.N):
			return None

                N = nparam.N;
                if(N < 2):
                   N = 2 

                for n in range(2, N+1):
		    for i in range(len(data) - (n-1)):
			    val = data[i] 
			    for j in range(1, n):
				    val += data[i+j]
                            #key = hashlib.sha1(val.encode('utf-8'))
			    hash_tb[val.encode('utf-8')] = hash_tb[val.encode('utf-8')] + 1;

		#sort reversely
		hash_tb = dict(sorted(hash_tb.items(), key=operator.itemgetter(1), reverse=True))

		counter = 0
		for key in (hash_tb.iterkeys()):

			profile[key] = counter;
			if(counter >= nparam.K):
				break;

			counter = counter + 1;

                #print("words are: "+str(data)+"\n")
                #print(str(profile))

		return profile
				

	def train(self, mode):
		for key in self.cate_data.iterkeys():
			self.cur_data = self.cate_data[key] 
			self.profiles[key] = self.__build_profile()

	def infer(self, vector):
		self.cur_data = vector;
		inf_vect = self.__build_profile()
		
                abs_min = sys.maxint
		for key in self.profiles.iterkeys():
			hash_dict = self.profiles[key];
			if(hash_dict is None):
                                language = None
				break

			sum_cost = 0;
                        for inf_key in inf_vect.iterkeys():
                            if(inf_key in hash_dict.keys()):
                                cost = abs(hash_dict[inf_key] - inf_vect[inf_key])
                            else:
                                cost = nparam.N * 10;
                            sum_cost = sum_cost + cost

                        if(sum_cost < abs_min):
                            abs_min = sum_cost;
                            language = key;

		return language 

	def show(self):
		print(self.profiles)
			
		
	def save_model(self, modename):
                self.profiles["N"] = nparam.N
                self.profiles["K"] = nparam.K
                pickle.dump(self.profiles, open(modename, 'w'))

	def load_model(self, modename):
                self.profiles = pickle.load(open(modename, 'r'))
                nparam.N = self.profiles["N"] 
                nparam.K = self.profiles["K"] 
